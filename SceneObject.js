const InitialMatrix = require('./Matrix/Initial'),
    LookAtMatrix = require('./Matrix/LookAt'),
    getRadFromDeg = require('./Utils/getRadFromDeg');

function getSceneObject(modifiers){
    if (modifiers && !Array.isArray(modifiers)) {
        modifiers = [modifiers];
    }
    return class SceneObject {
        constructor(position) {
            this._matrix = null;
            this._defaults = {};
            this._values = {};
            this._cachedMatrixHash = null;
            if(modifiers) {
                modifiers.forEach(modifier => {
                    for (let dName in modifier.defaults ) {
                        this._defaults[dName] = modifier.defaults[dName];
                    }
                });
            }
        }
        _setDefaults(defaultValues) {
            this._defaults = defaultValues;
        }
        _getHash(valueSet) {
            let k = '';
            for (let key in valueSet) {
                let value = Array.isArray(valueSet[key])
                    ? valueSet[key].join('|')
                    : valueSet[key];
                k += `${key}:${value};`;
            }
            return k;
        }
        set(position) {
            let nValues = {};
            if(position) {
                for(let key in this._defaults) {
                    nValues[key] = position[key] === undefined
                        ? this._defaults[key]
                        : position[key];
                }
            } else {
                nValues = this._defaults;
            }
            let hash = this._getHash(nValues);
            // console.log(hash);
            if (this._cachedMatrixHash !== hash) {
                this._cachedMatrixHash = hash;
                this._values = nValues;
                this._matrix = this.calculateMatrix();
            }
            return this._matrix;
        }
        calculateMatrix() {
            let matrix = new InitialMatrix();
            if(modifiers) {
                modifiers.forEach(modifier => {
                    matrix = modifier.calculateMatrix(matrix, this._values);
                });
            }
            return matrix;
        }
        getMatrix() {
            return this._matrix || this.set();
        }
    };
}

getSceneObject.ROTATABLE = {
    defaults: {
        rotationX: 0,
        rotationY: 0,
        rotationZ: 0
    },
    calculateMatrix(matrix, values) {
        if (values.rotationX) {
            matrix.xRotate(getRadFromDeg(values.rotationX));
        }
        if (values.rotationY) {
            matrix.yRotate(getRadFromDeg(values.rotationY));
        }
        if (values.rotationZ) {
            matrix.zRotate(getRadFromDeg(values.rotationZ));
        }
        return matrix;
    }
};

getSceneObject.SCALABLE = {
    defaults: {
        scaleX: 1,
        scaleY: 1,
        scaleZ: 1
    },
    calculateMatrix(matrix, values) {
        if(values.scaleX !== 1 || values.scaleY !== 1 || values.scaleZ !== 1) {
            matrix.scale(values.scaleX, values.scaleY, values.scaleZ);
        }
        return matrix;
    }
};
getSceneObject.TRANSLATABLE = {
    defaults: {
        x: 0,
        y: 0,
        z: 0
    },
    calculateMatrix(matrix, values) {
        if (values.x || values.y || values.z) {
            matrix.translate(values.x, values.y, values.z);
        }
        return matrix;
    }
};
getSceneObject.LOOKATABLE = {
    defaults: {
        subject: null,
        target: null,
        up: null
    },
    calculateMatrix(matrix, values) {
        if (values && (values.subject || values.target || values.up)) {
            matrix.multiply(new LookAtMatrix(
                values.subject || [0, 0, 0],
                values.target || [0, 0, 0],
                values.up || [0, 1, 0]
            ));
        }
        return matrix;
    }
};

module.exports = getSceneObject;
