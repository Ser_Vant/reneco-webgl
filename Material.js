module.exports = class Material {
    constructor(params) {
        this.name = null;
        this.diffuseColor = null;
        this.set(params);
    }
    set(params) {
        for (let aName in params) {
            if(this.hasOwnProperty(aName)) {
                this[aName] = params[aName];
            }
        }
    }
};
