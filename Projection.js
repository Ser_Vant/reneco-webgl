import SceneObject from './SceneObject';
import Perspective from './Matrix/Perspective';
import getRadFromDeg from './Utils/getRadFromDeg';

export default class Projection extends SceneObject() {
    constructor(position) {
        super();
        this._setDefaults({
            fieldOfView: 60,
            aspect: 0.75,
            zNear: 1,
            zFar: 2000
        });
        this.set(position);
    }
    calculateMatrix() {
        return new Perspective(
            getRadFromDeg(this._values.fieldOfView),
            this._values.aspect,
            this._values.zNear,
            this._values.zFar
        );
    }
}
