import SceneObject from './SceneObject';

import vShader from './Shaders/Shader.vs';
import fShader from './Shaders/Shader.fs';
import Vector from './Vector';
import Projection from './Projection';
import getRadFromDeg from './Utils/getRadFromDeg';

import Transpose from './Matrix/Transpose';

export default class Scene extends SceneObject(SceneObject.ROTATABLE) {
    constructor(models, camera, position) {
        super(position);
        this._canvasSize = {
            width: 0,
            height: 0
        };
        this.models = models;
        this.camera = camera;
        this.projection = new Projection({
            fieldOfView: position && position.fieldOfView,
            aspect: this._canvasSize.width / (this._canvasSize.height || 1),
            zNear: position && position.zNear,
            zFar: position && position.zFar
        });
        this.modelBuffers = {};
        this.locations = {};
        this.set(position);
    }
    _createProgram(shaders) {
        let program = this.gl.createProgram();
        (shaders || []).forEach(sh => {
            this.gl.attachShader(program, sh);
        });
        this.gl.linkProgram(program);
        let success = this.gl.getProgramParameter(program, this.gl.LINK_STATUS);
        if (success) {
            return program;
        }
        this.gl.deleteProgram(program);
    }
    _createShader(type, source) {
        if (this.gl) {
            let shader = this.gl.createShader(type);   // создание шейдера
            this.gl.shaderSource(shader, source);      // устанавливаем шейдеру его программный код
            this.gl.compileShader(shader);             // компилируем шейдер
            let success = this.gl.getShaderParameter(shader, this.gl.COMPILE_STATUS);
            if (success) {                        // если компиляция прошла успешно - возвращаем шейдер
                return shader;
            }
            console.log(this.gl.getShaderInfoLog(shader));
            this.gl.deleteShader(shader);
        }
    }
    attachCanvas($canvas) {
        this.$canvas = $canvas;
        this.gl = this.$canvas.getContext('webgl');
        if(this.gl) {
            this._program = this._createProgram([
                this._createShader(this.gl.VERTEX_SHADER, vShader),
                this._createShader(this.gl.FRAGMENT_SHADER, fShader),
            ]);

            // gl locations
            this.locations.a_position = this.gl.getAttribLocation(this._program, 'a_position');
            this.locations.a_normal = this.gl.getAttribLocation(this._program, 'a_normal');
            this.locations.a_color = this.gl.getAttribLocation(this._program, 'a_color');

            // this.locations.u_matrix = this.gl.getUniformLocation(this._program, 'u_matrix');
            this.locations.u_worldViewProjection = this.gl.getUniformLocation(this._program, 'u_worldViewProjection');
            this.locations.u_worldInverseTranspose = this.gl.getUniformLocation(this._program, 'u_worldInverseTranspose');
            this.locations.u_reverseLightDirection = this.gl.getUniformLocation(this._program, 'u_reverseLightDirection');

            // clear gl
            this.gl.clearColor(0, 0, 0, 0);
            this.gl.clear(this.gl.COLOR_BUFFER_BIT | this.gl.DEPTH_BUFFER_BIT);

            // load buffers
            this.models.forEach(model => {
                this.modelBuffers[model.name] = {
                    position: this.gl.createBuffer(),
                    color: this.gl.createBuffer(),
                    normal: this.gl.createBuffer()
                };
                this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.modelBuffers[model.name].position);
                this.gl.bufferData(
                    this.gl.ARRAY_BUFFER,
                    new Float32Array(model.getPositions()),
                    this.gl.STATIC_DRAW
                );

                this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.modelBuffers[model.name].color);
                this.gl.bufferData(
                    this.gl.ARRAY_BUFFER,
                    new Uint8Array(model.getColors()),
                    this.gl.STATIC_DRAW
                );
                this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.modelBuffers[model.name].normal);
                this.gl.bufferData(
                    this.gl.ARRAY_BUFFER,
                    new Float32Array(model.getNormals()),
                    this.gl.STATIC_DRAW
                );
            });
        }
    }
    setCanvasSize(size) {
        if (
            size.width !== this._canvasSize.width
            || size.height !== this._canvasSize.height
        ) {
            this._canvasSize.width = parseInt(size.width);
            this._canvasSize.height = parseInt(size.height);
            this.$canvas.width = this._canvasSize.width;
            this.$canvas.height = this._canvasSize.height;
            this.projection.set({
                aspect: this._canvasSize.width / (this._canvasSize.height || 1)
            });
            this.draw();
        }
    }
    draw() {
        let gl = this.gl;
        gl.viewport(0, 0, gl.drawingBufferWidth, gl.drawingBufferHeight);
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

        gl.enable(gl.CULL_FACE);
        gl.enable(gl.DEPTH_TEST);
        gl.useProgram(this._program);

        let viewMatrix = this.camera.getMatrix().clone().inverse();

        let viewProjectionMatrix = this.projection.getMatrix()
            .clone()
            .multiply(viewMatrix);

        let worldMatrix = this.getMatrix().clone();

        let worldViewProjectionMatrix = viewProjectionMatrix
            .clone()
            .multiply(worldMatrix);

        gl.uniformMatrix4fv(this.locations.u_worldViewProjection, false, worldViewProjectionMatrix.toArray());
        gl.uniform3fv(this.locations.u_reverseLightDirection, new Vector(0.5, 0.7, 1).normalize().toArray());
        gl.uniformMatrix4fv(
            this.locations.u_worldInverseTranspose,
            false,
            new Transpose(worldMatrix.inverse()).toArray()
        );

        this.models.forEach(model => {
            // location
            gl.enableVertexAttribArray(this.locations.a_position);
            gl.bindBuffer(gl.ARRAY_BUFFER, this.modelBuffers[model.name].position);
            gl.vertexAttribPointer(this.locations.a_position, 3, gl.FLOAT, false, 0, 0);

            // // normal
            gl.enableVertexAttribArray(this.locations.a_normal);
            gl.bindBuffer(gl.ARRAY_BUFFER, this.modelBuffers[model.name].normal);
            gl.vertexAttribPointer(this.locations.a_normal, 3, gl.FLOAT, false, 0, 0);

            // color
            gl.enableVertexAttribArray(this.locations.a_color);
            gl.bindBuffer(gl.ARRAY_BUFFER, this.modelBuffers[model.name].color);
            gl.vertexAttribPointer(this.locations.a_color, 3, gl.UNSIGNED_BYTE, true, 0, 0);

            // gl.uniform3fv(this.locations.u_reverseLightDirection, new Vector(0.1, 0.7, 1).normalize().toArray());

            gl.drawArrays(gl.TRIANGLES, 0, model.vertexes.length);
        });
    }
}
