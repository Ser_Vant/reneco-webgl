module.exports = angleInRadians => {
    let c = Math.cos(angleInRadians),
        s = Math.sin(angleInRadians);

    return [
        c, s, 0, 0,
        -s, c, 0, 0,
        0, 0, 1, 0,
        0, 0, 0, 1,
    ];
};
