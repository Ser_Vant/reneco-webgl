import Matrix from './Matrix';

import getTranslationMatrix from './getTranslation';

export default class Translation extends Matrix {
    constructor(tx, ty, tz) {
        super(getTranslationMatrix(tx, ty, tz));
    }
}
