import Matrix from './Matrix';

import getXRotation from './getXRotation';

export default class RotateX extends Matrix {
    constructor(angle) {
        super(getXRotation(angle));
    }
}
