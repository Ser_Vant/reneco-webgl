module.exports = angleInRadians => {
    let c = Math.cos(angleInRadians),
        s = Math.sin(angleInRadians);

    return [
        c, 0, -s, 0,
        0, 1, 0, 0,
        s, 0, c, 0,
        0, 0, 0, 1,
    ];
};
