import Matrix from './Matrix';

export default class Transpose extends Matrix {
    constructor(matrix) {
        super([
            matrix.value(0), matrix.value(4), matrix.value(8),  matrix.value(12),
            matrix.value(1), matrix.value(5), matrix.value(9),  matrix.value(13),
            matrix.value(2), matrix.value(6), matrix.value(10), matrix.value(14),
            matrix.value(3), matrix.value(7), matrix.value(11), matrix.value(15),
        ]);
    }
}
