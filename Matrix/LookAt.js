const Matrix = require('./Matrix'),
    Vector = require('../Vector');

module.exports = class LookAt extends Matrix {
    constructor(cameraPosition, target, up) {
        cameraPosition = new Vector(cameraPosition);
        let zAxis = cameraPosition.subtract(new Vector(target)).normalize(),
            xAxis = new Vector(up).cross(zAxis),
            yAxis = zAxis.cross(xAxis);
        super([
            xAxis.x,          xAxis.y,          xAxis.z,          0,
            yAxis.x,          yAxis.y,          yAxis.z,          0,
            zAxis.x,          zAxis.y,          zAxis.z,          0,
            cameraPosition.x, cameraPosition.y, cameraPosition.z, 1
        ]);
    }
};
