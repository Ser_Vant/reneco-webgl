import Matrix from './Matrix';

import getYRotation from './getYRotation';

export default class RotateY extends Matrix {
    constructor(angle) {
        super(getYRotation(angle));
    }
}
