import Matrix from './Matrix';

import getScale from './getScale';

export default class Scale extends Matrix {
    constructor(sx, sy, sz) {
        super(getScale(sx, sy, sz));
    }
}
