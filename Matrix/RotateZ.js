import Matrix from './Matrix';

import getZRotation from './getZRotation';

export default class RotateZ extends Matrix {
    constructor(angle) {
        super(getZRotation(angle));
    }
}
