module.exports = class Vector {
    static parseLine(line) {
        return new Vector(line.split(/\s/));
    }
    constructor(x, y, z, w) {
        if (x instanceof Vector) {
            return x;
        } else if(Array.isArray(x)) {
            w = x[3];
            z = x[2];
            y = x[1];
            x = x[0];
        }
        this.x = x || 0;
        this.y = y || 0;
        this.z = z || 0;
        this.w = w || 0;
    }
    subtract(vectorB) {
        return new Vector(
            this.x - vectorB.x,
            this.y - vectorB.y,
            this.z - vectorB.z,
            this.w - vectorB.w
        );
    }
    cross(vectorB) {
        return new Vector(
            this.y * vectorB.z - this.z * vectorB.y,
            this.z * vectorB.x - this.x * vectorB.z,
            this.x * vectorB.y - this.y * vectorB.x
        );
    }
    normalize() {
        let length = Math.sqrt(this.x * this.x + this.y * this.y + this.z * this.z + this.w * this.w);
        // make sure we don't divide by 0.
        if (length > 0.00001) {
            return new Vector(
                this.x / length,
                this.y / length,
                this.z / length,
                this.w / length
            );
        } else {
            return new Vector();
        }
    }
    toArray(length = 3) {
        let ret = [],
            attrs = ['x', 'y', 'z', 'w'],
            aL = length || attrs.length;
        for (let i = 0; i < aL; i++) {
            ret.push(this[attrs[i]]);
        }
        return ret;
    }
};
