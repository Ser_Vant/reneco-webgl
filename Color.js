const Vector = require('./Vector');
module.exports = class Color extends Vector {
    static parseLine(line) {
        return Color.parseFloat.apply(null, line.split(/[\s|,]+/).map(component => {
            return parseFloat(component);
        }));
    }
    static parseFloat(r, g, b) {
        return new Color([r, g, b].map(component => {
            return Math.max(0, Math.min(255, parseInt(Math.floor(component * 256.0))));
        }));
    }
};
