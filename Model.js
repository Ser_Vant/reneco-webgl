const SceneObject = require('./SceneObject'),
    Vector = require('./Vector'),
    Material = require('./Material'),
    Color = require('./Color'),

    DEFAULT_COLOR = new Vector(100, 255, 255);

module.exports = class Model extends SceneObject([
    SceneObject.ROTATABLE,
    SceneObject.SCALABLE,
    SceneObject.TRANSLATABLE,
    SceneObject.LOOKATABLE
]){
    constructor(modelData, position) {
        super();
        this.name = modelData && modelData.name;
        this.vertexes = [];

        if (modelData && modelData.position) {
            for(let i=0; i < modelData.position.length; i = i + 9) { // Face loop
                this.addSurface(
                    new Vector(modelData.position.slice(i, i + 3)),
                    new Vector(modelData.position.slice(i + 3, i + 6)),
                    new Vector(modelData.position.slice(i + 6, i + 9)),
                    new Material({
                        diffuseColor: new Color(modelData.color.slice(i, i + 3))
                    })
                );
            }
        }
    }
    _getArray(name) {
        return this.vertexes.reduce((prev, cur) => {
            Array.prototype.push.apply(
                prev,
                cur[name] && cur[name].toArray()
            );
            return prev;
        }, []);
    }
    getPositions() {
        return this._getArray('position');
    }
    getColors() {
        return this._getArray('color');
    }
    getNormals() {
        return this._getArray('normal');
    }
    updateVertex(ix, vertexData) {
        if(ix !== undefined && vertexData) {
            if(!this.vertexes[ix]) {
                this.vertexes[ix] = {
                    position: null,
                    color: null,
                    normals: null
                };
            }
            for(let attr in vertexData) {
                this.vertexes[ix][attr] = vertexData[attr];
            }
        }
    }
    addVertex(vertexData) {
        if(
            vertexData.position
            || vertexData.texture
            || vertexData.normal
            || vertexData.color
        ) {
            this.vertexes.push({
                position: new Vector(vertexData.position),
                color: new Vector(vertexData.color || DEFAULT_COLOR),
                normal: vertexData.normal
                    ? new Vector(vertexData.normal)
                    : null
            });
        }
    }
    addSurface(a, b, c, material) {
        let face = [
                new Vector(a),
                new Vector(b),
                new Vector(c)
            ],
            vecP = face[1].subtract(face[0]),
            vecQ = face[2].subtract(face[0]),
            VecN = vecP.cross(vecQ).normalize();
        for (let j = 0; j < face.length; j++) {
            this.addVertex({
                position: face[j],
                color: material && new Color(material.diffuseColor),
                normal: VecN
            });
        }
    }
    getMaterial() {
        return [];
    }
    stringify() {
        return JSON.stringify({
            name: this.name,
            position: this.getPositions(),
            normal: this.getNormals(),
            color: this.getColors(),
            material: this.getMaterial()
        });
    }
};
