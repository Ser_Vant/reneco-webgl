import React from 'react';

import './style.sass';
import Model from './Model';
import Camera from './Camera';
import Scene from './Scene';

import fLetter from './Fletter';

export default class WebGLViewer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            width: 0,
            height: 0
        };
        this.models = (props.loadModels || [])
            .map(modelData => {
                return new Model(modelData);
            });

        // this.models = [];
        // this.models.push(new Model(fLetter));

        this.camera = new Camera({
            subject: [0, 0, 10],
            target: [0, 0, 0],
            up: [0, 1, 0]
        });
        this.scene = new Scene(this.models, this.camera, {
            rotationY: 360,
            rotationX: 360
        });
    }
    onMouseMove = event => {
        this.scene.set({
            rotationY: event.pageX / this.state.width * 90 + 315,
            rotationX: event.pageY / this.state.height * 90 + 315
        });
        this.scene.draw();
    }
    onWheel = event => {

    }
    componentDidMount() {
        window.addEventListener('resize', this.resize);
        this.scene.attachCanvas(this.$canvas);
        this.scene.draw();
        this.resize();
    }
    componentWillUnmount() {
        window.removeEventListener('resize', this.resize);
    }
    resize = () => {
        let size = {
            width: this.$box.offsetWidth,
            height: this.$box.offsetHeight
        };
        if (size.width !== this.state.width || size.height !== this.state.height) {
            this.setState(size);
            this.scene && this.scene.setCanvasSize(size);
        }
    }
    redraw() {
        this.scene && this.scene.draw();
    }
    render() {
        return (<div
            className='webgl'
            ref={ref => { this.$box = ref; }}
        >
            <canvas
                onWheel={this.onWheel}
                onMouseMove={this.onMouseMove}
                style={{
                    width: this.state.width,
                    height: this.state.height
                }}
                ref={ref => { this.$canvas = ref; }}
            />
        </div>);
    }
}
