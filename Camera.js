import SceneObject from './SceneObject';

export default class Camera extends SceneObject(SceneObject.LOOKATABLE) {
    constructor(position) {
        super(position);
        this._setDefaults({
            subject: position && position.subject || [100, 150, 200],
            target: position && position.target || [0, 35, 0],
            up: position && position.up || [0, 1, 0]
        });
        this.set(position);
    }
}
