module.exports = deg => {
    return deg * Math.PI / 180;
};
