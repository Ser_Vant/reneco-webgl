const fs = require('fs'),
    FBXParser = require('./fbx/index'),
    OBJParser = require('./obj/parser'),
    path = require('path');

module.exports = {
    parse(url) {
        switch(path.extname(url).toLowerCase()) {
        case '.fbx':
            return this.parseFBX(url);
        case '.obj':
            return this.parseOBJ(url);
        default:
            return Promise.reject(Error(`Unknown filetype ${path.extname(url)}`));
        }
    },
    parseOBJ(url) {
        return OBJParser(url);
    },
    parseFBX(url) {
        return new Promise((resolve, reject) => {
            fs.readFile(path.resolve(url), (err, FBXBuffer) => {
                if (err) reject(Error(err));
                let AB = FBXBuffer.buffer.slice(FBXBuffer.byteOffset, FBXBuffer.byteOffset + FBXBuffer.byteLength);
                resolve(FBXParser(AB));
            });
        });
    }
};
