const lineRegexp = /^([^\s]+)\s+(.*)$/;
module.exports = line => {
    let match = lineRegexp.exec(line);
    return {
        type: match && match[1],
        value: match && match[2]
    };
};
