const ReadFile = require('./readFile'),
    Material = require('../../Material'),
    Color = require('../../Color'),
    Vector = require('../../Vector');
module.exports = url => {
    return new Promise((resolve, reject) => {
        let materials = {},
            curMaterial = null;
        const flush = () => {
            if (curMaterial) {
                materials[curMaterial.name] = curMaterial;
            }
            curMaterial = null;
        };
        ReadFile(url, line => {
            switch(line.type) {
            case 'newmtl':
                flush();
                curMaterial = new Material({ name: line.value });
                break;
            case 'Kd':
                curMaterial.set({
                    diffuseColor: Color.parseLine(line.value)
                });
            }
        }, () => {
            flush();
            resolve(materials);
        });
    });
};
