module.exports = (line, material) => {
    return line.split(/\s+/).map( vertexIx => {
        let vs = vertexIx.split('/');
        return {
            vertex: vs[0] !== undefined && +vs[0] || null,
            texture: vs[1] !== undefined && +vs[1]|| null,
            normal: vs[2] !== undefined && +vs[2]|| null,
            material: material
        };
    });
};
