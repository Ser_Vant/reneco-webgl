const ReadLine = require('readline'),
    fs = require('fs'),
    parseLine = require('./parseLine');

module.exports = (url, callback, endCallback) => {
    const ReadLineStream = ReadLine.createInterface({
        input: fs.createReadStream(url)
    });
    ReadLineStream.on('line', raw => {
        callback && callback(parseLine(raw));
    });
    ReadLineStream.on('close', raw => {
        callback && endCallback();
    });
};
