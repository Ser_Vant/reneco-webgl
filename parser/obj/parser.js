const path = require('path'),
    Model = require('../../Model'),
    Vector = require('../../Vector'),
    parseLine = require('./parseLine'),
    parseMaterialLib = require('./parseMaterials'),
    parseFace = require('./parseFace'),
    readFile = require('./readFile'),
    createModel = require('./createModel');

module.exports = url => {
    return new Promise((resolve, reject) => {
        let modelName = null,
            MaterialLib = null,
            vertexes = [],
            normales = [],
            textures = [],
            faces = [],
            ModelList = [],
            material = null;
        const flushModel = () => {
            let ret = ((mN, f, v, n, t) => {
                return MaterialLib
                    .then(materials => {
                        if(mN) {
                            ModelList.push(createModel(
                                mN,
                                f,
                                v,
                                n,
                                t,
                                materials
                            ));
                        }
                    });
            })(
                modelName,
                faces,
                vertexes,
                normales,
                textures
            );
            modelName = null;
            faces = [];
            material = null;
            return ret;
        };
        readFile(url, line => {
            switch(line.type) {
            case 'mtllib':
                MaterialLib = parseMaterialLib(path.join(path.dirname(url), line.value));
                break;
            case 'o':
                modelName && flushModel().catch(e => console.log(e, 'EEEEEEEEEEEEE'));
                modelName = line.value;
                break;
            case 'v':
                vertexes.push(line.value.split(/\s/).map(v => {
                    return parseFloat(v);
                }));
                break;
            case 'f':
                faces.push(parseFace(line.value, material));
                break;
            case 'usemtl':
                material = line.value;
                break;
            }

        }, () => {
            Promise.all([
                flushModel(),
                MaterialLib
            ]).then(data => {
                resolve({
                    models: ModelList,
                    materials: data[1]
                });
            }).catch(e => {
                console.error(e + '!!!!!!!!!!!!!!!!!!')
            });
        });
    });
};
