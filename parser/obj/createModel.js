const Model = require('../../Model'),
    Vector = require('../../Vector');

module.exports = (modelName, faces, vertexes, normales, textures, materials) => {

    let model = new Model();
    model.name = modelName;
    faces.forEach(face => {
        if (
            vertexes[face[0].vertex-1]
            && vertexes[face[1].vertex-1]
            && vertexes[face[2].vertex-1]
        ) {
            model.addSurface(
                new Vector(vertexes[face[0].vertex-1]),
                new Vector(vertexes[face[1].vertex-1]),
                new Vector(vertexes[face[2].vertex-1]),
                materials[face[0].material]
            );
        }
    });

    return model;
};
