/**
 * @param {textureNode} textureNode - Node to get texture information from.
 * @param {THREE.TextureLoader} loader
 * @param {Map<number, string(image blob URL)>} imageMap
 * @param {Map<number, {parents: {ID: number, relationship: string}[], children: {ID: number, relationship: string}[]}>} connections
 * @returns {THREE.Texture}
 */
module.exports = function parseTexture(textureNode, loader, imageMap, connections) {

    var FBX_ID = textureNode.id;

    var name = textureNode.name;

    var fileName;

    var filePath = textureNode.properties.FileName;
    var relativeFilePath = textureNode.properties.RelativeFilename;

    var children = connections.get(FBX_ID).children;

    if (children !== undefined && children.length > 0 && imageMap.has(children[0].ID)) {

        fileName = imageMap.get(children[0].ID);

    } else if (relativeFilePath !== undefined && relativeFilePath[0] !== '/' &&
        relativeFilePath.match(/^[a-zA-Z]:/) === null) {

        // use textureNode.properties.RelativeFilename
        // if it exists and it doesn't seem an absolute path

        fileName = relativeFilePath;

    } else {

        var split = filePath.split(/[\\\/]/);

        if (split.length > 0) {

            fileName = split[split.length - 1];

        } else {

            fileName = filePath;

        }

    }

    var currentPath = loader.path;

    if (fileName.indexOf('blob:') === 0) {

        loader.setPath(undefined);

    }

    /**
     * @type {THREE.Texture}
     */
    var texture = {}; // loader.load(fileName);
    // texture.name = name;
    // texture.FBX_ID = FBX_ID;

    // var wrapModeU = textureNode.properties.WrapModeU;
    // var wrapModeV = textureNode.properties.WrapModeV;

    // var valueU = wrapModeU !== undefined ? wrapModeU.value : 0;
    // var valueV = wrapModeV !== undefined ? wrapModeV.value : 0;

    // // http://download.autodesk.com/us/fbx/SDKdocs/FBX_SDK_Help/files/fbxsdkref/class_k_fbx_texture.html#889640e63e2e681259ea81061b85143a
    // // 0: repeat(default), 1: clamp

    // texture.wrapS = valueU === 0 ? THREE.RepeatWrapping : THREE.ClampToEdgeWrapping;
    // texture.wrapT = valueV === 0 ? THREE.RepeatWrapping : THREE.ClampToEdgeWrapping;

    // loader.setPath(currentPath);

    return texture;

}