const parseColor = require('./parseColor');

/**
 * @typedef {{Diffuse: FBXVector3, Specular: FBXVector3, Shininess: FBXValue, Emissive: FBXVector3, EmissiveFactor: FBXValue, Opacity: FBXValue}} FBXMaterialProperties
 */
/**
 * @typedef {{color: THREE.Color=, specular: THREE.Color=, shininess: number=, emissive: THREE.Color=, emissiveIntensity: number=, opacity: number=, transparent: boolean=, map: THREE.Texture=}} THREEMaterialParameterPack
 */
/**
 * @param {FBXMaterialProperties} properties
 * @param {Map<number, THREE.Texture>} textureMap
 * @param {{ID: number, relationship: string}[]} childrenRelationships
 * @returns {THREEMaterialParameterPack}
 */
module.exports = function parseParameters(properties, textureMap, childrenRelationships) {

    let parameters = {};

    if (properties.DiffuseColor) {
        parameters.color = parseColor(properties.DiffuseColor);
    }
    if (properties.SpecularColor) {
        parameters.specular = parseColor(properties.SpecularColor);
    }
    if (properties.Shininess) {
        parameters.shininess = properties.Shininess.value;
    }
    if (properties.EmissiveColor) {
        parameters.emissive = parseColor(properties.EmissiveColor);
    }
    if (properties.EmissiveFactor) {
        parameters.emissiveIntensity = properties.EmissiveFactor.value;
    }
    if (properties.Opacity) {
        parameters.opacity = properties.Opacity.value;
    }
    if (parameters.opacity < 1.0) {
        parameters.transparent = true;
    }

    for (var childrenRelationshipsIndex = 0, childrenRelationshipsLength = childrenRelationships.length; childrenRelationshipsIndex < childrenRelationshipsLength; ++childrenRelationshipsIndex) {

        var relationship = childrenRelationships[childrenRelationshipsIndex];

        var type = relationship.relationship;

        switch (type) {

        case "DiffuseColor":
        case " \"DiffuseColor":
            parameters.map = textureMap.get(relationship.ID);
            break;

        case "Bump":
        case " \"Bump":
            parameters.bumpMap = textureMap.get(relationship.ID);
            break;

        case "NormalMap":
        case " \"NormalMap":
            parameters.normalMap = textureMap.get(relationship.ID);
            break;

        case " \"AmbientColor":
        case " \"EmissiveColor":
        case "AmbientColor":
        case "EmissiveColor":
        default:
            console.warn('Unknown texture application of type ' + type + ', skipping texture');
            break;

        }

    }

    return parameters;

};
