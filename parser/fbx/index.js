const BinaryParser = require('./BinaryParser'),
    parseConnections = require('./parseConnections'),
    parseImages = require('./parseImages'),
    parseTextures = require('./parseTextures'),
    parseMaterials = require('./parseMaterials'),
    parseDeformers = require('./parseDeformers'),
    parseGeometries = require('./parseGeometries'),
    parseScene = require('./parseScene');

module.exports = binaryData => {
    let FBXTree = new BinaryParser().parse(binaryData),
        connections = parseConnections(FBXTree),
        images = parseImages(FBXTree),
        textures = parseTextures(FBXTree, null, images, connections),
        materials = parseMaterials(FBXTree, textures, connections),
        deformers = parseDeformers(FBXTree, connections),
        geometryMap = parseGeometries(FBXTree, connections, deformers);
    return parseScene(FBXTree, connections, deformers, geometryMap, materials);
};
