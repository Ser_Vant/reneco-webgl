module.exports = class Triangle {
    constructor() {
        /**
         * @type {{position: THREE.Vector3, normal: THREE.Vector3, uv: THREE.Vector2, skinIndices: THREE.Vector4, skinWeights: THREE.Vector4}[]}
         */
        this.vertices = [];
    }
    copy(target) {
        let returnVar = target || new Triangle();
        for(let i = 0; i< this.vertices.length; ++i) {
            this.vertices[i].copy(returnVar.vertices[i]);
        }
        return returnVar;
    }
    flattenToBuffers(vertexBuffer, normalBuffer, uvBuffer, colorBuffer, skinIndexBuffer, skinWeightBuffer) {
        let vertices = this.vertices;
        for (let i = 0, l = vertices.length; i < l; ++i) {
            vertices[i].flattenToBuffers(vertexBuffer, normalBuffer, uvBuffer, colorBuffer, skinIndexBuffer, skinWeightBuffer);
        }
    }
};
