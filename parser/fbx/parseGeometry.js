const parseMeshGeometry = require('./parseMeshGeometry'),
    parseNurbsGeometry = require('./parseNurbsGeometry');
/**
 * Generates BufferGeometry from FBXGeometryNode.
 * @param {FBXGeometryNode} geometryNode
 * @param {{parents: {ID: number, relationship: string}[], children: {ID: number, relationship: string}[]}} relationships
 * @param {Map<number, {map: Map<number, {FBX_ID: number, indices: number[], weights: number[], transform: number[], transformLink: number[], linkMode: string}>, array: {FBX_ID: number, indices: number[], weights: number[], transform: number[], transformLink: number[], linkMode: string}[]}>} deformers
 * @returns {THREE.BufferGeometry}
 */
module.exports = function parseGeometry(geometryNode, relationships, deformers) {

    switch (geometryNode.attrType) {

        case 'Mesh':
        return parseMeshGeometry(geometryNode, relationships, deformers);
        break;

        case 'NurbsCurve':
        return parseNurbsGeometry(geometryNode);
        break;

    }

}