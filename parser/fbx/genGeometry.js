const parseFloatArray = require('./parseFloatArray'),
    parseIntArray = require('./parseIntArray'),
    Geometry = require('./Geometry'),
    getNormals = require('./getNormals'),
    getUVs = require('./getUVs'),
    getColors = require('./getColors'),
    getMaterials = require('./getMaterials'),
    Vertex = require('./Vertex'),
    getData = require('./getData'),
    Face = require('./Face');
/**
 * @param {{map: Map<number, {FBX_ID: number, indices: number[], weights: number[], transform: number[], transformLink: number[], linkMode: string}>, array: {FBX_ID: number, indices: number[], weights: number[], transform: number[], transformLink: number[], linkMode: string}[]}} deformer - Skeleton representation for geometry instance.
 * @returns {THREE.BufferGeometry}
 */
module.exports = function genGeometry(geometryNode, deformer) {

    let geometry = new Geometry(),
        subNodes = geometryNode.subNodes,
        // First, each index is going to be its own vertex.
        vertexBuffer = parseFloatArray(subNodes.Vertices.properties.a),
        indexBuffer = parseIntArray(subNodes.PolygonVertexIndex.properties.a),
        normalInfo,
        uvInfo,
        colorInfo,
        materialInfo;

    if (subNodes.LayerElementNormal) {
        normalInfo = getNormals(subNodes.LayerElementNormal[0]);
    }
    if (subNodes.LayerElementUV) {
        uvInfo = getUVs(subNodes.LayerElementUV[0]);
    }
    if (subNodes.LayerElementColor) {
        colorInfo = getColors(subNodes.LayerElementColor[0]);
    }
    if (subNodes.LayerElementMaterial) {
        materialInfo = getMaterials(subNodes.LayerElementMaterial[0]);
    }

    let faceVertexBuffer = [],
        polygonIndex = 0;

    for (let polygonVertexIndex = 0; polygonVertexIndex < indexBuffer.length; polygonVertexIndex++) {
        var vertexIndex = indexBuffer[polygonVertexIndex];
        var endOfFace = false;
        if (vertexIndex < 0) {
            vertexIndex = vertexIndex ^ - 1;
            indexBuffer[polygonVertexIndex] = vertexIndex;
            endOfFace = true;
        }
        var vertex = new Vertex();
        var weightIndices = [];
        var weights = [];

        vertex.position = vertexBuffer.slice(vertexIndex * 3, vertexIndex * 3 + 3);
        if (deformer) {
            var subDeformers = deformer.map;
            for (var key in subDeformers) {
                var subDeformer = subDeformers[key];
                var indices = subDeformer.indices;
                for (var j = 0; j < indices.length; j++) {
                    var index = indices[j];
                    if (index === vertexIndex) {
                        weights.push(subDeformer.weights[j]);
                        weightIndices.push(subDeformer.index);
                        break;
                    }
                }
            }

            if (weights.length > 4) {
                console.warn('FBXLoader: Vertex has more than 4 skinning weights assigned to vertex.  Deleting additional weights.');
                var WIndex = [0, 0, 0, 0];
                var Weight = [0, 0, 0, 0];
                weights.forEach(function (weight, weightIndex) {
                    var currentWeight = weight;
                    var currentIndex = weightIndices[weightIndex];
                    Weight.forEach(function (comparedWeight, comparedWeightIndex, comparedWeightArray) {
                        if (currentWeight > comparedWeight) {

                            comparedWeightArray[comparedWeightIndex] = currentWeight;
                            currentWeight = comparedWeight;

                            var tmp = WIndex[comparedWeightIndex];
                            WIndex[comparedWeightIndex] = currentIndex;
                            currentIndex = tmp;
                        }
                    });
                });

                weightIndices = WIndex;
                weights = Weight;
            }

            for (var i = weights.length; i < 4; ++i) {
                weights[i] = 0;
                weightIndices[i] = 0;
            }

            vertex.skinWeights.fromArray(weights);
            vertex.skinIndices.fromArray(weightIndices);

        }

        if (normalInfo) {
            vertex.normal = getData(polygonVertexIndex, polygonIndex, vertexIndex, normalInfo);
        }

        if (uvInfo) {
            vertex.uv = getData(polygonVertexIndex, polygonIndex, vertexIndex, uvInfo);
        }

        if (colorInfo) {
            vertex.color = getData(polygonVertexIndex, polygonIndex, vertexIndex, colorInfo);
        }

        // console.log(vertex);

        faceVertexBuffer.push(vertex);

        if (endOfFace) {
            var face = new Face();
            face.genTrianglesFromVertices(faceVertexBuffer);

            if (materialInfo !== undefined) {
                var materials = getData(polygonVertexIndex, polygonIndex, vertexIndex, materialInfo);
                face.materialIndex = materials[0];
            } else {

                // Seems like some models don't have materialInfo(subNodes.LayerElementMaterial).
                // Set 0 in such a case.
                face.materialIndex = 0;
            }

            geometry.faces.push(face);
            faceVertexBuffer = [];
            polygonIndex++;
            endOfFace = false;
        }

    }

    /**
     * @type {{vertexBuffer: number[], normalBuffer: number[], uvBuffer: number[], skinIndexBuffer: number[], skinWeightBuffer: number[], materialIndexBuffer: number[]}}
     */
    var bufferInfo = geometry.flattenToBuffers();

    var geo = {};
    geo.name = geometryNode.name;

    geo.position = bufferInfo.vertexBuffer.slice();
    if (bufferInfo.normalBuffer.length > 0) {
        geo.normal = bufferInfo.normalBuffer.slice();
    }
    if (bufferInfo.uvBuffer.length > 0) {
        geo.uv = bufferInfo.uvBuffer.slice();
    }
    if (subNodes.LayerElementColor) {
        geo.color = bufferInfo.colorBuffer.slice();
    }

    if (deformer) {
        geo.skinIndex = bufferInfo.skinIndexBuffer.slice();
        geo.skinWeight = bufferInfo.skinWeightBuffer.slice();
        geo.FBX_Deformer = deformer;
    }

    // Convert the material indices of each vertex into rendering groups on the geometry.

    let materialIndexBuffer = bufferInfo.materialIndexBuffer,
        prevMaterialIndex = materialIndexBuffer[0],
        startIndex = 0;

    for (let j = 0; j < materialIndexBuffer.length; ++j) {
        if (materialIndexBuffer[j] !== prevMaterialIndex) {
            geo.groups = geo.groups || [];
            geo.groups.push({
                start: startIndex,
                count: j - startIndex,
                materialIndex: prevMaterialIndex
            });
            prevMaterialIndex = materialIndexBuffer[j];
            startIndex = j;
        }
    }
    return geo;
};
