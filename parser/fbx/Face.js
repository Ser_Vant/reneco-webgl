const append = require('./append'),
    Triangle = require('./Triangle');

module.exports = class Face {
    constructor() {
        /**
         * @type {{vertices: {position: THREE.Vector3, normal: THREE.Vector3, uv: THREE.Vector2, skinIndices: THREE.Vector4, skinWeights: THREE.Vector4}[]}[]}
         */
        this.triangles = [];
        this.materialIndex = 0;
    }
    copy(target) {
        let returnVar = target || new Face();
        for(let i = 0; i< this.triangles.length; ++i) {
            this.triangles[i].copy(returnVar.triangles[i]);
        }
        returnVar.materialIndex = this.materialIndex;
        return returnVar;
    }
    genTrianglesFromVertices(vertexArray) {
        for (let i = 2; i < vertexArray.length; ++i) {
            let triangle = new Triangle();
            triangle.vertices[0] = vertexArray[0];
            triangle.vertices[1] = vertexArray[i - 1];
            triangle.vertices[2] = vertexArray[i];
            this.triangles.push(triangle);
        }
    }

    flattenToBuffers(vertexBuffer, normalBuffer, uvBuffer, colorBuffer, skinIndexBuffer, skinWeightBuffer, materialIndexBuffer) {
        let triangles = this.triangles,
            materialIndex = this.materialIndex;
        for (let i = 0, l = triangles.length; i < l; ++i) {
            triangles[i].flattenToBuffers(vertexBuffer, normalBuffer, uvBuffer, colorBuffer, skinIndexBuffer, skinWeightBuffer);
            append(materialIndexBuffer, [materialIndex, materialIndex, materialIndex]);
        }
    }
};
