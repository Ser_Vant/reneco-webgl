const parseFloatArray = require('./parseFloatArray'),
    parseIntArray = require('./parseIntArray');
/**
 * Parses UV information for geometry.
 * @param {FBXGeometryNode} geometryNode
 * @returns {{dataSize: number, buffer: number[], indices: number[], mappingType: string, referenceType: string}}
 */
module.exports = function getUVs(UVNode) {
    let mappingType = UVNode.properties.MappingInformationType,
        referenceType = UVNode.properties.ReferenceInformationType,
        buffer = parseFloatArray(UVNode.subNodes.UV.properties.a),
        indexBuffer = [];
    if (referenceType === 'IndexToDirect') {
        indexBuffer = parseIntArray(UVNode.subNodes.UVIndex.properties.a);
    }

    return {
        dataSize: 2,
        buffer: buffer,
        indices: indexBuffer,
        mappingType: mappingType,
        referenceType: referenceType
    };
};
