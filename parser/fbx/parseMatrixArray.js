const parseFloatArray = require('./parseFloatArray');
module.exports = function parseMatrixArray(floatString) {
    return parseFloatArray(floatString);
}