const parseIntArray = require('./parseIntArray');
/**
 * Parses material application information for geometry.
 * @param {FBXGeometryNode}
 * @returns {{dataSize: number, buffer: number[], indices: number[], mappingType: string, referenceType: string}}
 */
module.exports = function getMaterials(MaterialNode) {

    let mappingType = MaterialNode.properties.MappingInformationType,
        referenceType = MaterialNode.properties.ReferenceInformationType;
    if (mappingType === 'NoMappingInformation') {
        return {
            dataSize: 1,
            buffer: [0],
            indices: [0],
            mappingType: 'AllSame',
            referenceType: referenceType
        };
    }

    let materialIndexBuffer = parseIntArray(MaterialNode.subNodes.Materials.properties.a),

        // Since materials are stored as indices, there's a bit of a mismatch between FBX and what
        // we expect.  So we create an intermediate buffer that points to the index in the buffer,
        // for conforming with the other functions we've written for other data.
        materialIndices = [];
    for (let materialIndexBufferIndex = 0, materialIndexBufferLength = materialIndexBuffer.length; materialIndexBufferIndex < materialIndexBufferLength; ++materialIndexBufferIndex) {
        materialIndices.push(materialIndexBufferIndex);
    }
    return {
        dataSize: 1,
        buffer: materialIndexBuffer,
        indices: materialIndices,
        mappingType: mappingType,
        referenceType: referenceType
    };
};
