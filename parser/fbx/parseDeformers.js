const parseSkeleton = require('./parseSkeleton');


/**
 * Generates map of Skeleton-like objects for use later when generating and binding skeletons.
 * @param {{Objects: {subNodes: {Deformer: Object.<number, FBXSubDeformerNode>}}}} FBXTree
 * @param {Map<number, {parents: {ID: number, relationship: string}[], children: {ID: number, relationship: string}[]}>} connections
 * @returns {Map<number, {map: Map<number, {FBX_ID: number, indices: number[], weights: number[], transform: number[], transformLink: number[], linkMode: string}>, array: {FBX_ID: number, indices: number[], weights: number[], transform: number[], transformLink: number[], linkMode: string}[], skeleton: THREE.Skeleton|null}>}
 */
module.exports = function parseDeformers(FBXTree, connections) {

    var deformers = {};

    if ('Deformer' in FBXTree.Objects.subNodes) {

        var DeformerNodes = FBXTree.Objects.subNodes.Deformer;

        for (var nodeID in DeformerNodes) {

        var deformerNode = DeformerNodes[nodeID];

        if (deformerNode.attrType === 'Skin') {

            var conns = connections.get(parseInt(nodeID));
            var skeleton = parseSkeleton(conns, DeformerNodes);
            skeleton.FBX_ID = parseInt(nodeID);

            deformers[nodeID] = skeleton;

        }

        }

    }

    return deformers;

}