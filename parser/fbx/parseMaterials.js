const parseMaterial = require('./parseMaterial');

/**
 * Parses map of Material information.
 * @param {{Objects: {subNodes: {Material: Object.<number, FBXMaterialNode>}}}} FBXTree
 * @param {Map<number, THREE.Texture>} textureMap
 * @param {Map<number, {parents: {ID: number, relationship: string}[], children: {ID: number, relationship: string}[]}>} connections
 * @returns {Map<number, THREE.Material>}
 */
module.exports = function parseMaterials(FBXTree, textureMap, connections) {

    var materialMap = new Map();

    if ('Material' in FBXTree.Objects.subNodes) {

        var materialNodes = FBXTree.Objects.subNodes.Material;
        for (var nodeID in materialNodes) {

            var material = parseMaterial(materialNodes[nodeID], textureMap, connections);
            materialMap.set(parseInt(nodeID), material);

        }

    }

    return materialMap;

}