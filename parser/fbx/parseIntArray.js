	/**
	 * Parses comma separated list of int numbers and returns them in an array.
	 * @example
	 * // Returns [ 5, 8, 2, 3 ]
	 * parseFloatArray( "5,8,2,3" )
	 * @returns {number[]}
	 */
module.exports = function parseIntArray(string) {

    var array = string.split(',');

    for (var i = 0, l = array.length; i < l; i++) {

        array[i] = parseInt(array[i]);

    }

    return array;

}