const append = require('./append');

module.exports = class FBXTree {
    add(key, val) {
        this[key] = val;
    }
    searchConnectionParent(id) {
        if (this.__cache_search_connection_parent === undefined) {
            this.__cache_search_connection_parent = [];
        }
        if (this.__cache_search_connection_parent[id] !== undefined) {
            return this.__cache_search_connection_parent[id];
        } else {
            this.__cache_search_connection_parent[id] = [];
        }
        let conns = this.Connections.properties.connections;
        let results = [];
        for (let i = 0; i < conns.length; ++i) {
            if (conns[i][0] == id) {
                // 0 means scene root
                let res = conns[i][1] === 0 ? - 1 : conns[i][1];
                results.push(res);
            }
        }
        if (results.length > 0) {
            append(this.__cache_search_connection_parent[id], results);
            return results;
        } else {
            this.__cache_search_connection_parent[id] = [- 1];
            return [- 1];
        }
    }
    searchConnectionChildren(id) {
        if (this.__cache_search_connection_children === undefined) {
            this.__cache_search_connection_children = [];
        }
        if (this.__cache_search_connection_children[id] !== undefined) {
            return this.__cache_search_connection_children[id];
        } else {
            this.__cache_search_connection_children[id] = [];
        }
        let conns = this.Connections.properties.connections;
        let res = [];
        for (let i = 0; i < conns.length; ++i) {
            if (conns[i][1] == id) {
                // 0 means scene root
                res.push(conns[i][0] === 0 ? - 1 : conns[i][0]);
                // there may more than one kid, then search to the end
            }
        }
        if (res.length > 0) {
            append(this.__cache_search_connection_children[id], res);
            return res;
        } else {
            this.__cache_search_connection_children[id] = [];
            return [];
        }
    }
    searchConnectionType(id, to) {
        let key = id + ',' + to; // TODO: to hash
        if (this.__cache_search_connection_type === undefined) {
            this.__cache_search_connection_type = {};
        }
        if (this.__cache_search_connection_type[key] !== undefined) {
            return this.__cache_search_connection_type[key];
        } else {
            this.__cache_search_connection_type[key] = '';
        }
        let conns = this.Connections.properties.connections;
        for (let i = 0; i < conns.length; ++i) {
            if (conns[i][0] == id && conns[i][1] == to) {
            // 0 means scene root
                this.__cache_search_connection_type[key] = conns[i][2];
                return conns[i][2];
            }
        }
        this.__cache_search_connection_type[id] = null;
        return null;
    }
};
