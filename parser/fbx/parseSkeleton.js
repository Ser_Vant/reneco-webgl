const parseIntArray = require('./parseIntArray'),
    parseFloatArray = require('./parseFloatArray'),
    parseMatrixArray = require('./parseMatrixArray');
/**
 * Generates a "Skeleton Representation" of FBX nodes based on an FBX Skin Deformer's connections and an object containing SubDeformer nodes.
 * @param {{parents: {ID: number, relationship: string}[], children: {ID: number, relationship: string}[]}} connections
 * @param {Object.<number, FBXSubDeformerNode>} DeformerNodes
 * @returns {{map: Map<number, {FBX_ID: number, indices: number[], weights: number[], transform: number[], transformLink: number[], linkMode: string}>, array: {FBX_ID: number, indices: number[], weights: number[], transform: number[], transformLink: number[], linkMode: string}[], skeleton: THREE.Skeleton|null}}
 */
module.exports = function parseSkeleton(connections, DeformerNodes) {
    let subDeformers = {},
        children = connections.children;
    for (let i = 0, l = children.length; i < l; ++i) {
        let child = children[i],
            subDeformerNode = DeformerNodes[child.ID],
            subDeformer = {
                FBX_ID: child.ID,
                index: i,
                indices: [],
                weights: [],
                transform: parseMatrixArray(subDeformerNode.subNodes.Transform.properties.a),
                transformLink: parseMatrixArray(subDeformerNode.subNodes.TransformLink.properties.a),
                linkMode: subDeformerNode.properties.Mode
            };
        if ('Indexes' in subDeformerNode.subNodes) {
            subDeformer.indices = parseIntArray(subDeformerNode.subNodes.Indexes.properties.a);
            subDeformer.weights = parseFloatArray(subDeformerNode.subNodes.Weights.properties.a);
        }
        subDeformers[child.ID] = subDeformer;
    }
    return {
        map: subDeformers,
        bones: []
    };
};
