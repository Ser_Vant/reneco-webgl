/**
 * An instance of a Vertex with data for drawing vertices to the screen.
 * @constructor
 */
module.exports = class Vertex {
    constructor() {

        /**
         * Position of the vertex.
         * @type {THREE.Vector3}
         */
        this.position = [];

        /**
         * Normal of the vertex
         * @type {THREE.Vector3}
         */
        this.normal = [];

        /**
         * UV coordinates of the vertex.
         * @type {THREE.Vector2}
         */
        this.uv = [];

        /**
         * Color of the vertex
         * @type {THREE.Vector3}
         */
        this.color = [];

        /**
         * Indices of the bones vertex is influenced by.
         * @type {THREE.Vector4}
         */
        this.skinIndices = [];

        /**
         * Weights that each bone influences the vertex.
         * @type {THREE.Vector4}
         */
        this.skinWeights = [];
    }
    copy(target) {
        let returnVar = target || new Vertex();
        returnVar.position = this.position.slice();
        returnVar.normal = this.normal.slice();
        returnVar.uv = this.uv.slice();
        returnVar.skinIndices = this.skinIndices.slice();
        returnVar.skinWeights = this.skinWeights.slice();
        return returnVar;
    }
    flattenToBuffers(vertexBuffer, normalBuffer, uvBuffer, colorBuffer, skinIndexBuffer, skinWeightBuffer) {
        Array.prototype.push.apply(vertexBuffer, this.position);
        Array.prototype.push.apply(normalBuffer, this.normal);
        Array.prototype.push.apply(uvBuffer, this.uv);
        Array.prototype.push.apply(colorBuffer, this.color);
        Array.prototype.push.apply(skinIndexBuffer, this.skinIndices);
        Array.prototype.push.apply(skinWeightBuffer, this.skinWeights);
    }
};
