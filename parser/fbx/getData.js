const slice = require('./slice'),
    dataArray = [],
    GetData = {
        ByPolygonVertex: {
            /**
             * Function uses the infoObject and given indices to return value array of object.
             * @param {number} polygonVertexIndex - Index of vertex in draw order (which index of the index buffer refers to this vertex).
             * @param {number} polygonIndex - Index of polygon in geometry.
             * @param {number} vertexIndex - Index of vertex inside vertex buffer (used because some data refers to old index buffer that we don't use anymore).
             * @param {{datasize: number, buffer: number[], indices: number[], mappingType: string, referenceType: string}} infoObject - Object containing data and how to access data.
             * @returns {number[]}
             */
            Direct: function (polygonVertexIndex, polygonIndex, vertexIndex, infoObject) {
                let from = (polygonVertexIndex * infoObject.dataSize),
                    to = (polygonVertexIndex * infoObject.dataSize) + infoObject.dataSize;
                // return infoObject.buffer.slice( from, to );
                return slice(dataArray, infoObject.buffer, from, to);
            },

            /**
             * Function uses the infoObject and given indices to return value array of object.
             * @param {number} polygonVertexIndex - Index of vertex in draw order (which index of the index buffer refers to this vertex).
             * @param {number} polygonIndex - Index of polygon in geometry.
             * @param {number} vertexIndex - Index of vertex inside vertex buffer (used because some data refers to old index buffer that we don't use anymore).
             * @param {{datasize: number, buffer: number[], indices: number[], mappingType: string, referenceType: string}} infoObject - Object containing data and how to access data.
             * @returns {number[]}
             */
            IndexToDirect: function (polygonVertexIndex, polygonIndex, vertexIndex, infoObject) {
                let index = infoObject.indices[polygonVertexIndex],
                    from = (index * infoObject.dataSize),
                    to = (index * infoObject.dataSize) + infoObject.dataSize;

                // return infoObject.buffer.slice( from, to );
                return slice(dataArray, infoObject.buffer, from, to);
            }
        },
        ByPolygon: {
            /**
             * Function uses the infoObject and given indices to return value array of object.
             * @param {number} polygonVertexIndex - Index of vertex in draw order (which index of the index buffer refers to this vertex).
             * @param {number} polygonIndex - Index of polygon in geometry.
             * @param {number} vertexIndex - Index of vertex inside vertex buffer (used because some data refers to old index buffer that we don't use anymore).
             * @param {{datasize: number, buffer: number[], indices: number[], mappingType: string, referenceType: string}} infoObject - Object containing data and how to access data.
             * @returns {number[]}
             */
            Direct: function (polygonVertexIndex, polygonIndex, vertexIndex, infoObject) {
                let from = polygonIndex * infoObject.dataSize,
                    to = polygonIndex * infoObject.dataSize + infoObject.dataSize;

                // return infoObject.buffer.slice( from, to );
                return slice(dataArray, infoObject.buffer, from, to);
            },

            /**
             * Function uses the infoObject and given indices to return value array of object.
             * @param {number} polygonVertexIndex - Index of vertex in draw order (which index of the index buffer refers to this vertex).
             * @param {number} polygonIndex - Index of polygon in geometry.
             * @param {number} vertexIndex - Index of vertex inside vertex buffer (used because some data refers to old index buffer that we don't use anymore).
             * @param {{datasize: number, buffer: number[], indices: number[], mappingType: string, referenceType: string}} infoObject - Object containing data and how to access data.
             * @returns {number[]}
             */
            IndexToDirect: function (polygonVertexIndex, polygonIndex, vertexIndex, infoObject) {
                let index = infoObject.indices[polygonIndex],
                    from = index * infoObject.dataSize,
                    to = index * infoObject.dataSize + infoObject.dataSize;

                // return infoObject.buffer.slice( from, to );
                return slice(dataArray, infoObject.buffer, from, to);
            }
        },

        ByVertice: {
            Direct: function (polygonVertexIndex, polygonIndex, vertexIndex, infoObject) {
                let from = (vertexIndex * infoObject.dataSize),
                    to = (vertexIndex * infoObject.dataSize) + infoObject.dataSize;

                // return infoObject.buffer.slice( from, to );
                return slice(dataArray, infoObject.buffer, from, to);
            }
        },
        AllSame: {
            /**
             * Function uses the infoObject and given indices to return value array of object.
             * @param {number} polygonVertexIndex - Index of vertex in draw order (which index of the index buffer refers to this vertex).
             * @param {number} polygonIndex - Index of polygon in geometry.
             * @param {number} vertexIndex - Index of vertex inside vertex buffer (used because some data refers to old index buffer that we don't use anymore).
             * @param {{datasize: number, buffer: number[], indices: number[], mappingType: string, referenceType: string}} infoObject - Object containing data and how to access data.
             * @returns {number[]}
             */
            IndexToDirect: function (polygonVertexIndex, polygonIndex, vertexIndex, infoObject) {
                let from = infoObject.indices[0] * infoObject.dataSize,
                    to = infoObject.indices[0] * infoObject.dataSize + infoObject.dataSize;

                // return infoObject.buffer.slice( from, to );
                return slice(dataArray, infoObject.buffer, from, to);
            }
        }
    };


module.exports = function getData(polygonVertexIndex, polygonIndex, vertexIndex, infoObject) {

    return GetData[infoObject.mappingType][infoObject.referenceType](polygonVertexIndex, polygonIndex, vertexIndex, infoObject);

};
