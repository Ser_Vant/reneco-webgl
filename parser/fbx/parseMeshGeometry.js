const genGeometry = require('./genGeometry');

/**
 * Specialty function for parsing Mesh based Geometry Nodes.
 * @param {FBXGeometryNode} geometryNode
 * @param {{parents: {ID: number, relationship: string}[], children: {ID: number, relationship: string}[]}} relationships - Object representing relationships between specific geometry node and other nodes.
 * @param {Map<number, {map: Map<number, {FBX_ID: number, indices: number[], weights: number[], transform: number[], transformLink: number[], linkMode: string}>, array: {FBX_ID: number, indices: number[], weights: number[], transform: number[], transformLink: number[], linkMode: string}[]}>} deformers - Map object of deformers and subDeformers by ID.
 * @returns {THREE.BufferGeometry}
 */
module.exports = function parseMeshGeometry(geometryNode, relationships, deformers) {

    for (var i = 0; i < relationships.children.length; ++i) {

        var deformer = deformers[relationships.children[i].ID];
        if (deformer !== undefined) break;

    }

    return genGeometry(geometryNode, deformer);

}