module.exports = function slice(a, b, from, to) {
    for (let i = from, j = 0; i < to; i++ , j++) {
        a[j] = b[i];
    }
    return a;
};
