const parseGeometry = require('./parseGeometry');

/**
 * Generates Buffer geometries from geometry information in FBXTree, and generates map of THREE.BufferGeometries
 * @param {{Objects: {subNodes: {Geometry: Object.<number, FBXGeometryNode}}}} FBXTree
 * @param {Map<number, {parents: {ID: number, relationship: string}[], children: {ID: number, relationship: string}[]}>} connections
 * @param {Map<number, {map: Map<number, {FBX_ID: number, indices: number[], weights: number[], transform: number[], transformLink: number[], linkMode: string}>, array: {FBX_ID: number, indices: number[], weights: number[], transform: number[], transformLink: number[], linkMode: string}[], skeleton: THREE.Skeleton|null}>} deformers
 * @returns {Map<number, THREE.BufferGeometry>}
 */
module.exports = function parseGeometries(FBXTree, connections, deformers) {

    var geometryMap = new Map();

    if ('Geometry' in FBXTree.Objects.subNodes) {

        var geometryNodes = FBXTree.Objects.subNodes.Geometry;

        for (var nodeID in geometryNodes) {

        var relationships = connections.get(parseInt(nodeID));
        var geo = parseGeometry(geometryNodes[nodeID], relationships, deformers);
        geometryMap.set(parseInt(nodeID), geo);

        }

    }

    return geometryMap;

}