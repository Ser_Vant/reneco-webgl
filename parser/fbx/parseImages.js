/**
 * Parses map of images referenced in FBXTree.
 * @param {{Objects: {subNodes: {Texture: Object.<string, FBXTextureNode>}}}} FBXTree
 * @returns {Map<number, string(image blob URL)>}
 */
module.exports = function parseImages(FBXTree) {

    /**
     * @type {Map<number, string(image blob URL)>}
     */
    var imageMap = new Map();

    if ('Video' in FBXTree.Objects.subNodes) {

        var videoNodes = FBXTree.Objects.subNodes.Video;

        for (var nodeID in videoNodes) {

            var videoNode = videoNodes[nodeID];

            // raw image data is in videoNode.properties.Content
            if ('Content' in videoNode.properties) {

                var image = parseImage(videoNodes[nodeID]);
                imageMap.set(parseInt(nodeID), image);

            }

        }

    }

    return imageMap;

}