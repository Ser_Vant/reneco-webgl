const parseFloatArray = require('./parseFloatArray');

/**
 * Specialty function for parsing NurbsCurve based Geometry Nodes.
 * @param {FBXGeometryNode} geometryNode
 * @param {{parents: {ID: number, relationship: string}[], children: {ID: number, relationship: string}[]}} relationships
 * @returns {THREE.BufferGeometry}
 */
module.exports = function parseNurbsGeometry(geometryNode) {

    if (THREE.NURBSCurve === undefined) {

        console.error("THREE.FBXLoader relies on THREE.NURBSCurve for any nurbs present in the model.  Nurbs will show up as empty geometry.");
        return new THREE.BufferGeometry();

    }

    var order = parseInt(geometryNode.properties.Order);

    if (isNaN(order)) {

        console.error("FBXLoader: Invalid Order " + geometryNode.properties.Order + " given for geometry ID: " + geometryNode.id);
        return new THREE.BufferGeometry();

    }

    var degree = order - 1;

    var knots = parseFloatArray(geometryNode.subNodes.KnotVector.properties.a);
    var controlPoints = [];
    var pointsValues = parseFloatArray(geometryNode.subNodes.Points.properties.a);

    for (var i = 0, l = pointsValues.length; i < l; i += 4) {

        controlPoints.push(new THREE.Vector4().fromArray(pointsValues, i));

    }

    var startKnot, endKnot;

    if (geometryNode.properties.Form === 'Closed') {

        controlPoints.push(controlPoints[0]);

    } else if (geometryNode.properties.Form === 'Periodic') {

        startKnot = degree;
        endKnot = knots.length - 1 - startKnot;

        for (var i = 0; i < degree; ++i) {

        controlPoints.push(controlPoints[i]);

        }

    }

    var curve = new THREE.NURBSCurve(degree, knots, controlPoints, startKnot, endKnot);
    var vertices = curve.getPoints(controlPoints.length * 7);

    var positions = new Float32Array(vertices.length * 3);

    for (var i = 0, l = vertices.length; i < l; ++i) {

        vertices[i].toArray(positions, i * 3);

    }

    var geometry = new THREE.BufferGeometry();
    geometry.addAttribute('position', new THREE.BufferAttribute(positions, 3));

    return geometry;

}