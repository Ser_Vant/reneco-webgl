const parseTexture = require('./parseTexture');

/**
 * Parses map of textures referenced in FBXTree.
 * @param {{Objects: {subNodes: {Texture: Object.<string, FBXTextureNode>}}}} FBXTree
 * @param {THREE.TextureLoader} loader
 * @param {Map<number, string(image blob URL)>} imageMap
 * @param {Map<number, {parents: {ID: number, relationship: string}[], children: {ID: number, relationship: string}[]}>} connections
 * @returns {Map<number, THREE.Texture>}
 */

module.exports = function parseTextures(FBXTree, loader, imageMap, connections) {

    /**
     * @type {Map<number, THREE.Texture>}
     */
    let textureMap = new Map();

    if ('Texture' in FBXTree.Objects.subNodes) {

        let textureNodes = FBXTree.Objects.subNodes.Texture;
        for (let nodeID in textureNodes) {
            let texture = parseTexture(textureNodes[nodeID], loader, imageMap, connections);
            textureMap.set(parseInt(nodeID), texture);
        }
    }
    return textureMap;
};
