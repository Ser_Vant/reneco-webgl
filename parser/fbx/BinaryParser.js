const BinaryReader = require('./BinaryReader'),
    FBXTree = require('./FBXTree'),
    Zlib = require('zlib');



module.exports = class BinaryParser {

    /**
     * Parses binary data and builds FBXTree as much compatible as possible with the one built by TextParser.
     * @param {ArrayBuffer} buffer
     * @returns {THREE.FBXTree}
     */
    parse(buffer) {

        let reader = new BinaryReader(buffer);
        reader.skip(23); // skip magic 23 bytes

        let version = reader.getUint32();

        console.log('FBX binary version: ' + version);

        let allNodes = new FBXTree();

        while (!this.endOfContent(reader)) {

            let node = this.parseNode(reader, version);
            if (node !== null) allNodes.add(node.name, node);

        }

        return allNodes;

    }

    /**
     * Checks if reader has reached the end of content.
     * @param {BinaryReader} reader
     * @returns {boolean}
     */
    endOfContent(reader) {

        // footer size: 160bytes + 16-byte alignment padding
        // - 16bytes: magic
        // - padding til 16-byte alignment (at least 1byte?)
        //   (seems like some exporters embed fixed 15bytes?)
        // - 4bytes: magic
        // - 4bytes: version
        // - 120bytes: zero
        // - 16bytes: magic
        if (reader.size() % 16 === 0) {
            return ((reader.getOffset() + 160 + 16) & ~0xf) >= reader.size();
        } else {
            return reader.getOffset() + 160 + 15 >= reader.size();
        }
    }

    /**
     * Parses Node as much compatible as possible with the one parsed by TextParser
     * TODO: could be optimized more?
     * @param {BinaryReader} reader
     * @param {number} version
     * @returns {Object} - Returns an Object as node, or null if NULL-record.
     */
    parseNode(reader, version) {

        // The first three data sizes depends on version.
        let endOffset = (version >= 7500) ? reader.getUint64() : reader.getUint32();
        let numProperties = (version >= 7500) ? reader.getUint64() : reader.getUint32();
        let propertyListLen = (version >= 7500) ? reader.getUint64() : reader.getUint32();
        let nameLen = reader.getUint8();
        let name = reader.getString(nameLen);

        // Regards this node as NULL-record if endOffset is zero
        if (endOffset === 0) return null;

        let propertyList = [];

        for (let i = 0; i < numProperties; i++) {
            propertyList.push(this.parseProperty(reader));
        }

        // console.log(propertyList);


        // Regards the first three elements in propertyList as id, attrName, and attrType
        let id = propertyList.length > 0 ? propertyList[0] : '';
        let attrName = propertyList.length > 1 ? propertyList[1] : '';
        let attrType = propertyList.length > 2 ? propertyList[2] : '';

        let subNodes = {};
        let properties = {};

        let isSingleProperty = false;

        // if this node represents just a single property
        // like (name, 0) set or (name2, [0, 1, 2]) set of {name: 0, name2: [0, 1, 2]}
        if (numProperties === 1 && reader.getOffset() === endOffset) {

            isSingleProperty = true;

        }

        while (endOffset > reader.getOffset()) {

            let node = this.parseNode(reader, version);

            if (node === null) continue;

            // special case: child node is single property
            if (node.singleProperty === true) {

                let value = node.propertyList[0];

                if (Array.isArray(value)) {

                // node represents
                //	Vertices: *3 {
                //		a: 0.01, 0.02, 0.03
                //	}
                // of text format here.

                    node.properties[node.name] = node.propertyList[0];
                    subNodes[node.name] = node;

                    // Later phase expects single property array is in node.properties.a as String.
                    // TODO: optimize
                    node.properties.a = value.toString();

                } else {

                // node represents
                // 	Version: 100
                // of text format here.

                    properties[node.name] = value;

                }

                continue;

            }

            // special case: connections
            if (name === 'Connections' && node.name === 'C') {

                let array = [];

                // node.propertyList would be like
                // ["OO", 111264976, 144038752, "d|x"] (?, from, to, additional values)
                for (let i = 1, il = node.propertyList.length; i < il; i++) {

                    array[i - 1] = node.propertyList[i];

                }

                if (properties.connections === undefined) {

                    properties.connections = [];

                }

                properties.connections.push(array);

                continue;

            }

            // special case: child node is Properties\d+
            if (node.name.match(/^Properties\d+$/)) {

                // move child node's properties to this node.

                let keys = Object.keys(node.properties);

                for (let i = 0, il = keys.length; i < il; i++) {
                    let key = keys[i];
                    properties[key] = node.properties[key];
                }

                continue;

            }

            // special case: properties
            if (name.match(/^Properties\d+$/) && node.name === 'P') {

                let innerPropName = node.propertyList[0];
                let innerPropType1 = node.propertyList[1];
                let innerPropType2 = node.propertyList[2];
                let innerPropFlag = node.propertyList[3];
                let innerPropValue;

                if (innerPropName.indexOf('Lcl ') === 0) innerPropName = innerPropName.replace('Lcl ', 'Lcl_');
                if (innerPropType1.indexOf('Lcl ') === 0) innerPropType1 = innerPropType1.replace('Lcl ', 'Lcl_');

                if (innerPropType1 === 'ColorRGB'
                    || innerPropType1 === 'Vector'
                    || innerPropType1 === 'Vector3D'
                    || innerPropType1.indexOf('Lcl_') === 0
                ) {

                    innerPropValue = [
                        node.propertyList[4],
                        node.propertyList[5],
                        node.propertyList[6]
                    ];

                } else {

                    innerPropValue = node.propertyList[4];

                }

                if (innerPropType1.indexOf('Lcl_') === 0) {

                    innerPropValue = innerPropValue.toString();

                }

                // this will be copied to parent. see above.
                properties[innerPropName] = {

                    'type': innerPropType1,
                    'type2': innerPropType2,
                    'flag': innerPropFlag,
                    'value': innerPropValue

                };

                continue;

            }

            // standard case
            // follows TextParser's manner.
            if (subNodes[node.name] === undefined) {

                if (typeof node.id === 'number') {

                    subNodes[node.name] = {};
                    subNodes[node.name][node.id] = node;

                } else {

                    subNodes[node.name] = node;

                }

            } else {

                if (node.id === '') {

                    if (!Array.isArray(subNodes[node.name])) {

                        subNodes[node.name] = [subNodes[node.name]];

                    }

                    subNodes[node.name].push(node);

                } else {

                    if (subNodes[node.name][node.id] === undefined) {

                        subNodes[node.name][node.id] = node;

                    } else {

                        // conflict id. irregular?

                        if (!Array.isArray(subNodes[node.name][node.id])) {

                            subNodes[node.name][node.id] = [subNodes[node.name][node.id]];

                        }

                        subNodes[node.name][node.id].push(node);

                    }

                }

            }

        }

        return {

            singleProperty: isSingleProperty,
            id: id,
            attrName: attrName,
            attrType: attrType,
            name: name,
            properties: properties,
            propertyList: propertyList, // raw property list, would be used by parent
            subNodes: subNodes

        };

    }
    parseProperty(reader) {

        let type = reader.getChar(),
            length,
            arrayLength,
            encoding,
            compressedLength,
            fl;

        switch (type) {

        case 'F':
            fl = reader.getFloat32();
            return fl;

        case 'D':
            fl = reader.getFloat64();
            return fl;

        case 'L':
            return reader.getInt64();

        case 'I':
            return reader.getInt32();

        case 'Y':
            return reader.getInt16();

        case 'C':
            return reader.getBoolean();

        case 'f':
        // case 'D':
        case 'd':
        case 'l':
        case 'i':
        case 'b':

            arrayLength = reader.getUint32();
            encoding = reader.getUint32(); // 0: non-compressed, 1: compressed
            compressedLength = reader.getUint32();

            if (encoding === 0) {

                switch (type) {
                case 'f':
                    return reader.getFloat32Array(arrayLength);
                case 'D':
                case 'd':
                    return reader.getFloat64Array(arrayLength);
                case 'l':
                    return reader.getInt64Array(arrayLength);
                case 'i':
                    return reader.getInt32Array(arrayLength);
                case 'b':
                    return reader.getBooleanArray(arrayLength);
                }

            }
            let inflate = Zlib.inflateSync(new Uint8Array(reader.getArrayBuffer(compressedLength)));
            let reader2 = new BinaryReader(inflate.buffer);
            switch (type) {

            case 'f':
                return reader2.getFloat32Array(arrayLength);

            case 'd':
            case 'D':
                return reader2.getFloat64Array(arrayLength);

            case 'l':
                return reader2.getInt64Array(arrayLength);

            case 'i':
                return reader2.getInt32Array(arrayLength);

            case 'b':
                return reader2.getBooleanArray(arrayLength);

            }

        case 'S':
            length = reader.getUint32();
            return reader.getString(length);

        case 'R':
            length = reader.getUint32();
            return reader.getArrayBuffer(length);

        default:
            throw new Error('FBXLoader: Unknown property type ' + type);
        }
    }

};
