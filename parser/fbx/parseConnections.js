/**
 * Parses map of relationships between objects.
 * @param {{Connections: { properties: { connections: [number, number, string][]}}}} FBXTree
 * @returns {Map<number, {parents: {ID: number, relationship: string}[], children: {ID: number, relationship: string}[]}>}
 */
module.exports = function parseConnections(FBXTree) {

    /**
     * @type {Map<number, { parents: {ID: number, relationship: string}[], children: {ID: number, relationship: string}[]}>}
     */
    var connectionMap = new Map();

    if ('Connections' in FBXTree) {

        /**
         * @type {[number, number, string][]}
         */
        var connectionArray = FBXTree.Connections.properties.connections;
        for (var connectionArrayIndex = 0, connectionArrayLength = connectionArray.length; connectionArrayIndex < connectionArrayLength; ++connectionArrayIndex) {

            var connection = connectionArray[connectionArrayIndex];

            if (!connectionMap.has(connection[0])) {

                connectionMap.set(connection[0], {
                    parents: [],
                    children: []
                });

            }

            var parentRelationship = { ID: connection[1], relationship: connection[2] };
            connectionMap.get(connection[0]).parents.push(parentRelationship);

            if (!connectionMap.has(connection[1])) {

                connectionMap.set(connection[1], {
                parents: [],
                children: []
                });

            }

            var childRelationship = { ID: connection[0], relationship: connection[2] };
            connectionMap.get(connection[1]).children.push(childRelationship);

        }

    }

    return connectionMap;

}