const parseParameters = require('./parseParameters');
/**
 * Takes information from Material node and returns a generated THREE.Material
 * @param {FBXMaterialNode} materialNode
 * @param {Map<number, THREE.Texture>} textureMap
 * @param {Map<number, {parents: {ID: number, relationship: string}[], children: {ID: number, relationship: string}[]}>} connections
 * @returns {THREE.Material}
 */
module.exports = function parseMaterial(materialNode, textureMap, connections) {
    let FBX_ID = materialNode.id,
        name = materialNode.attrName,
        type = materialNode.properties.ShadingModel;

    //Case where FBXs wrap shading model in property object.
    if (typeof type === 'object') {
        type = type.value;
    }

    let children = connections.get(FBX_ID).children,
        parameters = parseParameters(materialNode.properties, textureMap, children),
        material;

    switch (type.toLowerCase()) {

    case 'phong':
    case 'lambert':
        material = { type: type };
        break;
    default:
        console.warn('No implementation given for material type ' + type + ' in FBXLoader.js.  Defaulting to basic material');
        material = { type: 'basic', color: 0x3300ff };
        break;

    }

    console.log(parameters, name);
    for (let pName in parameters) {
        if(parameters.hasOwnProperty(pName)) {
            material[pName] = parameters[pName];
        }
    }
    material.name = name;

    return material;

};
