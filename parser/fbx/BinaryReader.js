module.exports = class BinaryReader {
    constructor(buffer, littleEndian) {
        this.dv = new DataView(buffer);
        this.offset = 0;
        this.littleEndian = (littleEndian !== undefined) ? littleEndian : true;
    }
    getOffset() {
        return this.offset;
    }
    size() {
        return this.dv.buffer.byteLength;
    }
    skip(length) {
        this.offset += length;
    }
    // seems like true/false representation depends on exporter.
    //   true: 1 or 'Y'(=0x59), false: 0 or 'T'(=0x54)
    // then sees LSB.
    getBoolean() {
        return (this.getUint8() & 1) === 1;
    }
    getBooleanArray(size) {
        let a = [];
        for (let i = 0; i < size; i++) {
            a.push(this.getBoolean());
        }
        return a;
    }
    getInt8 () {
        let value = this.dv.getInt8(this.offset);
        this.offset += 1;
        return value;
    }
    getInt8Array (size) {
        let a = [];
        for (let i = 0; i < size; i++) {
            a.push(this.getInt8());
        }
        return a;
    }
    getUint8() {
        let value = this.dv.getUint8(this.offset);
        this.offset += 1;
        return value;
    }
    getUint8Array(size) {
        let a = [];
        for (let i = 0; i < size; i++) {
            a.push(this.getUint8());
        }
        return a;
    }
    getInt16() {
        let value = this.dv.getInt16(this.offset, this.littleEndian);
        this.offset += 2;
        return value;
    }
    getInt16Array(size) {
        let a = [];
        for (let i = 0; i < size; i++) {
            a.push(this.getInt16());
        }
        return a;
    }
    getUint16() {
        let value = this.dv.getUint16(this.offset, this.littleEndian);
        this.offset += 2;
        return value;
    }
    getUint16Array(size) {
        let a = [];
        for (let i = 0; i < size; i++) {
            a.push(this.getUint16());
        }
        return a;
    }
    getInt32() {
        let value = this.dv.getInt32(this.offset, this.littleEndian);
        this.offset += 4;
        return value;
    }
    getInt32Array(size) {
        let a = [];
        for (let i = 0; i < size; i++) {
            a.push(this.getInt32());
        }
        return a;
    }
    getUint32() {
        let value = this.dv.getUint32(this.offset, this.littleEndian);
        this.offset += 4;
        return value;
    }
    getUint32Array(size) {
        let a = [];
        for (let i = 0; i < size; i++) {
            a.push(this.getUint32());
        }
        return a;
    }
    // JavaScript doesn't support 64-bit integer so attempting to calculate by ourselves.
    // 1 << 32 will return 1 so using multiply operation instead here.
    // There'd be a possibility that this method returns wrong value if the value
    // is out of the range between Number.MAX_SAFE_INTEGER and Number.MIN_SAFE_INTEGER.
    // TODO: safely handle 64-bit integer
    getInt64() {
        let low, high;
        if (this.littleEndian) {
            low = this.getUint32();
            high = this.getUint32();
        } else {
            high = this.getUint32();
            low = this.getUint32();
        }
        // calculate negative value
        if (high & 0x80000000) {
            high = ~high & 0xFFFFFFFF;
            low = ~low & 0xFFFFFFFF;
            if (low === 0xFFFFFFFF) high = (high + 1) & 0xFFFFFFFF;
            low = (low + 1) & 0xFFFFFFFF;
            return - (high * 0x100000000 + low);
        }
        return high * 0x100000000 + low;
    }
    getInt64Array(size) {
        let a = [];
        for (let i = 0; i < size; i++) {
            a.push(this.getInt64());
        }
        return a;
    }
    // Note: see getInt64() comment
    getUint64() {
        let low, high;
        if (this.littleEndian) {
            low = this.getUint32();
            high = this.getUint32();
        } else {
            high = this.getUint32();
            low = this.getUint32();
        }
        return high * 0x100000000 + low;
    }
    getUint64Array(size) {
        let a = [];
        for (let i = 0; i < size; i++) {
            a.push(this.getUint64());
        }
        return a;
    }
    getFloat32() {
        let value = this.dv.getFloat32(this.offset, this.littleEndian);
        this.offset += 4;
        return value;
    }
    getFloat32Array(size) {
        let a = [];
        for (let i = 0; i < size; i++) {
            a.push(this.getFloat32());
        }
        return a;
    }
    getFloat64() {
        let value = this.dv.getFloat64(this.offset, this.littleEndian);
        this.offset += 8;
        return value;
    }
    getFloat64Array(size) {
        let a = [];
        for (let i = 0; i < size; i++) {
            a.push(this.getFloat64());
        }
        return a;
    }
    getArrayBuffer(size) {
        let value = this.dv.buffer.slice(this.offset, this.offset + size);
        this.offset += size;
        return value;
    }
    getChar() {
        return String.fromCharCode(this.getUint8());
    }
    getString(size) {
        let s = '';
        while (size > 0) {
            let value = this.getUint8();
            size--;
            if (value === 0) break;
            s += String.fromCharCode(value);
        }
        this.skip(size);
        return s;
    }
};
