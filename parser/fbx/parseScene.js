
/**
 * Finally generates Scene graph and Scene graph Objects.
 * @param {{Objects: {subNodes: {Model: Object.<number, FBXModelNode>}}}} FBXTree
 * @param {Map<number, {parents: {ID: number, relationship: string}[], children: {ID: number, relationship: string}[]}>} connections
 * @param {Map<number, {map: Map<number, {FBX_ID: number, indices: number[], weights: number[], transform: number[], transformLink: number[], linkMode: string}>, array: {FBX_ID: number, indices: number[], weights: number[], transform: number[], transformLink: number[], linkMode: string}[], skeleton: THREE.Skeleton|null}>} deformers
 * @param {Map<number, THREE.BufferGeometry>} geometryMap
 * @param {Map<number, THREE.Material>} materialMap
 * @returns {THREE.Group}
 */
module.exports = function parseScene(FBXTree, connections, deformers, geometryMap, materialMap) {
    let ModelNode = FBXTree.Objects.subNodes.Model,
        models = [];

    // console.log(materialMap);
    for (let nodeID in ModelNode) {

        let id = parseInt(nodeID),
            node = ModelNode[nodeID],
            conns = connections.get(id),
            model = null;
        if(node.attrType === 'Mesh') {
            let materials = [],
                material,
                geometry;
            for (let childrenIndex = 0, childrenLength = conns.children.length; childrenIndex < childrenLength; ++childrenIndex) {
                let child = conns.children[childrenIndex];
                if (geometryMap.has(child.ID)) {
                    geometry = geometryMap.get(child.ID);
                }

                if (materialMap.has(child.ID)) {
                    materials.push(materialMap.get(child.ID));
                }
            }
            if (materials.length > 1) {
                material = materials;
            } else if (materials.length > 0) {
                material = materials[0];
            } else {
                material = null;
                materials.push(material);
            }
            models.push({
                geometry: geometry,
                material: material,
                FBX_ID: id,
                name: node.attrName.replace(/:/, '').replace(/_/, '').replace(/-/, '')
            });
        }
    }
    return models;
};
