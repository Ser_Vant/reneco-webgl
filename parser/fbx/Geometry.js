module.exports = class Geometry {
    constructor() {
        /**
         * @type {{triangles: {vertices: {position: THREE.Vector3, normal: THREE.Vector3, uv: THREE.Vector2, skinIndices: THREE.Vector4, skinWeights: THREE.Vector4}[]}[], materialIndex: number}[]}
         */
        this.faces = [];

        /**
         * @type {{}|THREE.Skeleton}
         */
        this.skeleton = null;
    }
    /**
     * @returns	{{vertexBuffer: number[], normalBuffer: number[], uvBuffer: number[], skinIndexBuffer: number[], skinWeightBuffer: number[], materialIndexBuffer: number[]}}
     */
    flattenToBuffers() {

        let vertexBuffer = [],
            normalBuffer = [],
            uvBuffer = [],
            colorBuffer = [],
            skinIndexBuffer = [],
            skinWeightBuffer = [],
            materialIndexBuffer = [],
            faces = this.faces;

        for(let i = 0, l = faces.length; i<l; ++i) {
            faces[i].flattenToBuffers(vertexBuffer, normalBuffer, uvBuffer, colorBuffer, skinIndexBuffer, skinWeightBuffer, materialIndexBuffer);
        }

        return {
            vertexBuffer: vertexBuffer,
            normalBuffer: normalBuffer,
            uvBuffer: uvBuffer,
            colorBuffer: colorBuffer,
            skinIndexBuffer: skinIndexBuffer,
            skinWeightBuffer: skinWeightBuffer,
            materialIndexBuffer: materialIndexBuffer
        };
    }
};

