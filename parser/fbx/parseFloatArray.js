module.exports = function parseFloatArray(string) {
    var array = string.split(',');
    for (var i = 0, l = array.length; i < l; i++) {
        array[i] = parseFloat(array[i]);
    }
    return array;
}