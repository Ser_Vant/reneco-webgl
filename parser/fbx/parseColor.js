function float2color(percentage) {
    var color_part_dec = 255 * percentage;
    var color_part_hex = Number(parseInt(color_part_dec, 10)).toString(16);
    return '#' + color_part_hex + color_part_hex + color_part_hex;
}

/**
 * Parses Color property from FBXTree.  Property is given as .value.x, .value.y, etc.
 * @param Float32 property - Property to parse as Color.
 * @returns {Object}
 */
module.exports = function parseColor(property) {

    return {
        raw: property.value,
        parsed: float2color(property.value)
    };
};
