const parseFloatArray = require('./parseFloatArray');

/**
 * Parses Vertex Color information for geometry.
 * @param {FBXGeometryNode} geometryNode
 * @returns {{dataSize: number, buffer: number[], indices: number[], mappingType: string, referenceType: string}}
 */
module.exports = function getColors(ColorNode) {
    let mappingType = ColorNode.properties.MappingInformationType,
        referenceType = ColorNode.properties.ReferenceInformationType,
        buffer = parseFloatArray(ColorNode.subNodes.Colors.properties.a),
        indexBuffer = [];
    if (referenceType === 'IndexToDirect') {
        indexBuffer = parseFloatArray(ColorNode.subNodes.ColorIndex.properties.a);
    }

    return {
        dataSize: 4,
        buffer: buffer,
        indices: indexBuffer,
        mappingType: mappingType,
        referenceType: referenceType
    };
};
