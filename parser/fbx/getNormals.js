const parseFloatArray = require('./parseFloatArray'),
    parseIntArray = require('./parseIntArray');

/**
 * Parses normal information for geometry.
 * @param {FBXGeometryNode} geometryNode
 * @returns {{dataSize: number, buffer: number[], indices: number[], mappingType: string, referenceType: string}}
 */
module.exports = function getNormals(NormalNode) {

    let mappingType = NormalNode.properties.MappingInformationType,
        referenceType = NormalNode.properties.ReferenceInformationType,
        buffer = parseFloatArray(NormalNode.subNodes.Normals.properties.a),
        indexBuffer = [];
    if (referenceType === 'IndexToDirect') {

        if ('NormalIndex' in NormalNode.subNodes) {

            indexBuffer = parseIntArray(NormalNode.subNodes.NormalIndex.properties.a);

        } else if ('NormalsIndex' in NormalNode.subNodes) {

            indexBuffer = parseIntArray(NormalNode.subNodes.NormalsIndex.properties.a);

        }

    }
    return {
        dataSize: 3,
        buffer: buffer,
        indices: indexBuffer,
        mappingType: mappingType,
        referenceType: referenceType
    };
};
